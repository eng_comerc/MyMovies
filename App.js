/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image} from 'react-native';
import MovieCard from "./src/shared/movie-card/movie-card";
import axios from "axios";

type Props = {};
export default class App extends Component<Props> {
  state = {
      movies: []
  }
  api_key = 'api_key=f6b9adefc9f1e328fc5a66a32f4a3354';
  base_path = 'https://api.themoviedb.org/3/';
  language = '&language=pt-BR';

  componentDidMount(){
      var url = `${this.base_path}movie/popular?${this.api_key}${this.language}`;
      url = "https://api.themoviedb.org/3/movie/popular?api_key=f6b9adefc9f1e328fc5a66a32f4a3354";
      axios.get(url).then(response => {
          this.setState({
              movies: response.data.results.map((m, i) => {
                return {
                  ...m,
                  key: m.id.toString()
                }
              })
          });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
            data={this.state.movies}
            renderItem={({item}) => <MovieCard movie={item}></MovieCard>}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardHeader: {

  },
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    paddingTop: 20
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
