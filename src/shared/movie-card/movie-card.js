import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';

export default class MovieCard extends Component<Props> {
    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center"
              }}>
                  <View style={{
                    height: 300
                  }}>
                      <Image 
                      resizeMode={'contain'} 
                      style={{flex:1, height: 300, width: 300}}
                      source={{uri: 'https://image.tmdb.org/t/p/w500/' + this.props.movie.poster_path}}
                    />
                  </View>
                  <View>
                    <Text style={{
                        fontSize: 20
                    }}>{this.props.movie.title}</Text>
                  </View>
              </View>
        )
    }
}

const styles = StyleSheet.create({
    cardHeader: {
  
    },
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF',
      paddingTop: 20
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });